# Requirements

# syndication
## concert budget weapon
 - user must publish record as a blog and RSS git
## vessel satoshi traffic
 - user must publish record as a tg channel
## offer person super
 - user must update dataset branch metadata for pedigree

# basic csvs requirements
## little patient night
- user must import a dataset with git clone
## talent rabbit region
- user must export a dataset with git push
## elegant divert glad
- user must authenticate with git HTTP smart service
## cream piano tooth
- user must authenticate with git HTTP dumb service
## sort multiply rice
- user must authenticate with gitlab over OAuth
## exercise fragile cage
- user must authenticate with github over OAuth
## riot side foil
- user must authenticate with gitea over OAuth
## limb renew scale
- desktop user must open dataset locally
## ribbon second oak
- desktop user must open multiple windows
## detect victory grow
- browser user must see a warning that clearing cache will destroy data 
## brisk turkey number
- user must download zip archive of the dataset
