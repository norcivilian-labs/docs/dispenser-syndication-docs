# dispenser-docs

Documentation for [syndication dispenser](https://gitlab.com/norcivilian-labs/dispenser), built with [mdBook](https://github.com/rust-lang/mdBook) and hosted at [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).
